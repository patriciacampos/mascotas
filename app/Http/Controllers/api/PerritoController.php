<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Perritos;
use Illuminate\Support\Facades\DB;

class PerritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $perritos = Perritos::orderBy('id', 'desc')->get();

        return response()->json($perritos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $countNombres = Perritos::select('*')->where('nombre',$request->nombre)->count();

        if($countNombres>0)
        {
            return response()->json([
                'error' => true,
                'message' => 'El nombre del perrito ya existe'
            ]);
        }
        else
        {
            try{
                $perrito = new Perritos();
    
                $perrito->nombre = $request->nombre;
                $perrito->color = $request->color;
                $perrito->raza = $request->raza;
                $perrito->sexo = $request->sexo;
                $perrito->activo = 1;
    
                $perrito->save();
                
    
                return response()->json([
                    'error' => false,
                    'message' => 'Perrito ingresado correctamente'
                ]); 
    
            }catch (\Exception $e){
                return response()->json([
                    'error' => true,
                    'message' => 'Error al ingresar perrito, intente nuevamente'
            ]);
            }
        }
        
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show(Request $request){ //buscar

        try{
            $perritos = Perritos::orderBy('id', 'desc')->get();
            return response()->json($perritos);

        }
        catch(\Exception $e)
        {
            return response()->json(array());
        }
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $perritos = Perritos::find($request->id);

            $perritos->nombre = $request->nombre;
            $perritos->color = $request->color;
            $perritos->raza = $request->raza;
            $perritos->sexo = $request->sexo;
            $perritos->activo = $request->activo;

            $perritos->save();

            return response()->json([
                'error' => false,
                'message' => 'Perrito actualizado correctamente'
            ]);

        }catch (\Exception $e){
            return response()->json([
                'error' => true,
                'message' => 'Error al editar perrito, intente nuevamente'
        ]);
        }
    }

    public function obtenerPerrito(Request $request)
    {
        try
        {
            $perritos = Perritos::findOrFail($request->id);
            return response()->json($perritos);

        }catch (\Exception $e){
            return response()->json([
                'error' => true,
                'message' => 'Error al ver perrito, intente nuevamente'
        ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $perritos = Perritos::find($request->id);

            $perritos->delete();

            return response()->json([
                'error' => false,
                'message' => 'Perrito eliminado correctamente'
            ]);
        }catch(Exception $e){
            return response()->json([
                'error' => true,
                'message' => 'Error al eliminar el perrito, intente nuevamente'
        ]);
        }
    
    }
}
