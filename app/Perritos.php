<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perritos extends Model{
    
    protected $table = 'perritos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}

