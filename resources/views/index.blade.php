<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 

<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>


<div class="container" style="padding: 5%">
<div class="form-control"style="align-content: 'right'; padding: 10px">
  <a id="btnAgregar" class="btn btn-primary">Agregar mascota</a>
</div>

<div class="form-control"style="align-content: 'center'; padding: 20px">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Codigo</th>
        <th scope="col">Nombre</th>
        <th scope="col">Color</th>
        <th scope="col">Raza</th>
        <th scope="col">Sexo</th>
        <th scope="col">Estatus</th>
        <th scope="col">Edición</th>
        <th scope="col">Eliminar</th>
      </tr>
    </thead>
    <tbody id="tbody">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
    </tbody>
  </table>
</div>
</div>
<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Agregar mascota</h5>
      </div>
      <div class="modal-body">
        <div class="form-control"style="align-content: 'center'; padding: 20px">
          <form>
              <h4>Datos</h4>
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" id="inpNombre" required>
              </div>
              <div class="form-group">
                <label>Color</label>
                <input type="text" class="form-control" id="inpColor" required>
              </div>
              <div class="form-group">
                  <label>Raza</label>
                  <input type="text" class="form-control" id="inpRaza" required>
              </div>
              <div class="form-group">
                <label>Sexo</label>
                <select class="form-select" id="inpSexo" required>
                  <option value="H">Hembra</option>
                  <option value="M">Macho</option>
                </select>
              </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="mdlClose" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnGuardarMascota" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo">Editar mascota</h5>
        <input type="hidden" name="hdnId" id="hdnIdE">
      </div>
      <div class="modal-body">
        <div class="form-control"style="align-content: 'center'; padding: 20px">
          <form>
              <h4>Datos</h4>
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" id="updateNombre" required>
              </div>
              <div class="form-group">
                <label>Color</label>
                <input type="text" class="form-control" id="updateColor" required>
              </div>
              <div class="form-group">
                  <label>Raza</label>
                  <input type="text" class="form-control" id="updateRaza" required>
              </div>
              <div class="form-group">
                <label>Sexo</label>
                <select class="form-select" id="updateSexo" required>
                  <option value="H">Hembra</option>
                  <option value="M">Macho</option>
                </select>
              </div>
              <div class="form-group">
                <label>Estado</label>
                <select class="form-select" id="updateActivo" required>
                  <option value="1">Habilitado</option>
                  <option value="0">Deshabilitado</option>
                </select>
              </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="mdlClose" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnUpdateMascota" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="titulo" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" name="hdnId" id="hdnId">
        <h5 class="modal-title" id="titulo">Confirmación</h5>
      </div>
      <div class="modal-body">
        <div class="form-control"style="align-content: 'center'; padding: 20px">
          <h5>¿Estás seguro que deseas eliminar la mascota?</h5>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="mdlClose" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="eliminarPerrito" class="btn btn-primary">Confirmar</button>
      </div>
    </div>
  </div>
</div>

<script>

  obtenerPerritos(); //Se cargan los datos en la tabla

  //AGREGAR MASCOTA
  $("#mdlClose").on('click',function(){
    $("#modalAgregar").modal('hide');
  })

  $("#btnAgregar").on('click',function(){
    $("#modalAgregar").modal('show');
    $("#inpNombre").val('');
    $("#inpColor").val('');
    $("#inpRaza").val('');
    $("#inpSexo").val('');
  })

  $("#btnGuardarMascota").on('click',function(){
    agregarPerrito();
  })

  //BOTON EDITAR MASCOTA
  $("body").delegate("#mdlClose", "click", function(){
    $("#modalEditar").modal('hide');
  });

  $("body").delegate("#editarPerrito", "click", function(){
    editarPerrito();
  });
  
  $("body").delegate("#btnUpdateMascota", "click", function(){
    editarPerrito();
  });

  //BOTON ELIMINAR
  $("body").delegate("#mdlClose", "click", function(){
    $("#modalEliminar").modal('hide');
  });

  $("body").delegate("#eliminarPerrito", "click", function(){
    eliminarPerrito();
  });


  function obtenerPerritos(){
    $("#tbody").empty(); 
    $.ajax({
      url: 'api/listarPerrito',
      type: 'GET',

      success: function(json) {

   //Se vacia en caso de que contenga info.
        for(var i = 0; i < json.length;i++){
          var html = '';
          var status = '';

          if(json[i]["activo"]==1)
          {
            html += '<tr class="table-success">';
            status = 'Habilitado';
          }
          else
          {
            html += '<tr class="table-danger">';
            status = 'Deshabilitado';
          }

          html += '<td>'+json[i]["id"]+'</td>'
          html += '<td>'+json[i]["nombre"]+'</td>'
          html += '<td>'+json[i]["color"]+'</td>'
          html += '<td>'+json[i]["raza"]+'</td>'
          html += '<td>'+(json[i]["sexo"]=='H' ? 'Hembra' : 'Macho') +'</td>'
          html += '<td>'+status+'</td>'
          html += '<td>'+'<a class="btn btn-warning" value="'+json[i]["id"]+'" onclick="modalEditarPerrito('+json[i]["id"]+')" id="btnEditar">Editar</a>'+'</td>'
          html += '<td>'+'<a class="btn btn-danger" value="'+json[i]["id"]+'" onclick="modalEliminarPerrito('+json[i]["id"]+')" id="btnEliminar" >Eliminar</a>'+'</td>'
          html += '</tr>'
          $("#tbody").append(html);

        }
      },
    error: function() {
      toastr.error('Error al obtener los datos');
    }
    })
  }

  function agregarPerrito(){
    var data = {      nombre : $('#inpNombre').val(), 
                      color : $('#inpColor').val(),
                      raza : $('#inpRaza').val(),
                      sexo : $('#inpSexo').val() };

    $.ajax({
      url: 'api/agregarPerrito',
      data: data,
      type: 'POST',
      success:function(response){
        if(response.error==false){
          toastr.success('Exito!'+' '+response.message);
          $("#modalAgregar").modal('hide');
          obtenerPerritos();

        }
        else
        {
          toastr.warning('Atencion,'+' '+response.message);
        }
      },
      error: function() {
       toastr.error('Error al obtener los datos, por favor, vuelva a intentarlo mas tarde');
      }
    })
  }

  function modalEliminarPerrito(id)
  {
    $("#modalEliminar").modal('show');
    $("#hdnId").val(id);
  }

  function modalEditarPerrito(id)
  {
    $("#modalEditar").modal('show');
    $("#hdnIdE").val(id);

    var data = {id: id};
    $.ajax({
      url: 'api/obtenerPerrito',
      type: 'GET',
      data: data,
      success: function(json) {
        var data = { nombre : $('#updateNombre').val(json.nombre), 
                      color : $('#updateColor').val(json.color),
                      raza : $('#updateRaza').val(json.raza),
                      sexo : $('#updateSexo').val(json.sexo),
                      activo : $('#updateActivo').val(json.activo) };
      },
      error: function() {
        toastr.error('Error al obtener los datos');
      }
    })

  }  

  function eliminarPerrito()
  {
    var id = $("#hdnId").val(); 
    var data = {id: id};
    $.ajax({
      url: 'api/eliminarPerrito',
      type: 'DELETE',
      data: data,
      success: function(json) {
        toastr.success('Perrito eliminado correctamente');
        $("#modalEliminar").modal('hide');
        obtenerPerritos();
      },
      error: function() {
        toastr.error('Error al obtener los datos');
      }
    })
  }

  function editarPerrito(){
    var data = { id : $("#hdnIdE").val(),
                      nombre : $('#updateNombre').val(), 
                      color : $('#updateColor').val(),
                      raza : $('#updateRaza').val(),
                      sexo : $('#updateSexo').val(), 
                      activo : $('#updateActivo').val() };

    $.ajax({
      url: 'api/actualizarPerrito',
      data: data,
      type: 'PUT',
      success:function(response){
        $("#modalEditar").modal('hide');
        toastr.success('Perrito actualizado correctamente');
        obtenerPerritos();
      },
      error: function() {
        toastr.error('Error al obtener los datos');
      }
    })
  }

</script>
