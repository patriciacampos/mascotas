<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('perrito', 'api\PerritoController')->only([
    'index'
]);

Route::post('/agregarPerrito', 'api\PerritoController@store'); //Insertar POST

Route::get('/listarPerrito', 'api\PerritoController@show'); //GET Ver todos

Route::get('/obtenerPerrito', 'api\PerritoController@obtenerPerrito'); //GET Ver uno

Route::put('/actualizarPerrito', 'api\PerritoController@update'); //PUT actualizar

Route::delete('/eliminarPerrito', 'api\PerritoController@destroy'); //DELETE eliminar
